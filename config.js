module.exports = {
    mongoURI: process.env.K_DB_URI,
  
    // secretOrKey: process.env.SECRET,
    adminsecretOrKey: process.env.ADMIN_SECRET,
  
    secretOrKey: process.env.SECRET,

    googleSecretKey: process.env.GOOGLE_SECRET_KEY,
  
  
  }