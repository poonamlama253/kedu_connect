const express = require('express');
const router = express.Router();
const pickUpModule = require('./../../modules/pickUp/pickUpController');
const validate = require('./../../modules/pickUp/pickUpValidation');
const uploader = require("./../../helper/uploadHelper");
const roleValidation = require('./../../helper/roleValidationHelper');

module.exports = (router, passport) => {

    // @desc ------ start of admin routes  ------

    //create pickup request 
    router.post('/merchant/request/pickUp', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin', 'merchant']), validate.validateInput, pickUpModule.createPickUpRequest);


    // @desc ------ end of admin routes  ------
    

    // @desc ------ start of admin/merchant routes  ------

    //list pickup request 
    router.get('/user/list/pickUpRequest', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin', 'merchant']), pickUpModule.pickUpList);

    //get pickup request details
    router.get('/user/details/pickUpRequest/:pickUp_id', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin', 'merchant']), pickUpModule.pickUpDetails);

    //update pickup details
    router.patch('/user/update/pickUpRequest/:pickUp_id', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin', 'merchant']), validate.validateInput, pickUpModule.updatePickUpRequest);

    //delete pickup details
    router.delete('/user/delete/pickUpRequest/:pickUp_id', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin', 'merchant']), pickUpModule.deletePickUpRequest);


    // @desc ------ end of admin/merchant routes  ------


}