const express = require('express');
const router = express.Router();
const userModule = require('./../../modules/user/userController');
const validate = require('./../../modules/user/userValidation');
const uploader = require("./../../helper/uploadHelper");
const roleValidation = require('./../../helper/roleValidationHelper');

module.exports = (router, passport) => {

    // @desc ------ start of admin routes  ------

    //create super admin 
    router.post('/admin/add', userModule.addUser);

    //super-admin or admin create user 
    router.post('/admin/createUser', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin']), validate.sanitizeRegister, validate.checkDataDuplicate, validate.validateRegisterInput, userModule.createUser);

    //super-admin or admin get user list
    router.get('/admin/userList', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin']),  userModule.userList);


    // @desc ------ end of admin routes  ------
    




    // @desc ------ start of common routes  ------

    //merchant sign up 
    router.post('/user/singUp', validate.sanitizeRegister, validate.checkDataDuplicate, validate.validateRegisterInputSelf, userModule.signUp);

    //login user
    router.post('/user/login', validate.loginInput, userModule.login);

    //logout user
    router.post('/user/logout', passport.authenticate('bearer', { session: false }), userModule.logout);
    
    //validate otp 
    router.post('/user/validateOtp', validate.validateOtpInput, userModule.otpVerification);

    // @desc ------ end of common routes  ------


}