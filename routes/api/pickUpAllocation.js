const express = require('express');
const router = express.Router();
const pickUpAllocationModule = require('../../modules/pickUpAllocation/pickUpAllocationController');
const validate = require('../../modules/pickUpAllocation/pickUpAllocationValiation');
const uploader = require("./../../helper/uploadHelper");
const roleValidation = require('./../../helper/roleValidationHelper');

module.exports = (router, passport) => {

    // @desc ------ start of admin routes  ------

    //create pickup allocation 
    router.post('/admin/assign/rider/:pickUp_id', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin']), validate.validateInput, pickUpAllocationModule.assignRider);

    //get pickup allocation list
    router.get('/pickupAllocation/rider/list', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin', 'merchant', 'rider']), pickUpAllocationModule.pickUpAllocationList);

    //get pickup allocation details
    router.get('/pickupAllocation/rider/:pickUpAllocation_id', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin', 'merchant', 'rider']), pickUpAllocationModule.pickUpAllocationDetails);

    //update pickup allocation detials
    router.patch('/update/pickupAllocation/:pickUpAllocation_id', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin']) ,validate.validateUpdateInput, pickUpAllocationModule.updatePickUpAllocation);

    //delete pickup allocation
    router.delete('/delete/pickupAllocation/:pickUpAllocation_id', passport.authenticate('bearer', { session: false }), roleValidation(['super-admin', 'admin']) , pickUpAllocationModule.deletePickUpAllocation);


    // @desc ------ end of admin routes  ------
    

    // @desc ------ start of admin/merchant routes  ------

    
    // @desc ------ end of admin/merchant routes  ------


}