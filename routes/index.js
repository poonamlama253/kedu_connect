const Router = require('express').Router;
const passport = require('passport');
const userRoutes = require('./api/user')
const pickUpRoutes = require('./api/pickUp')
const pickUpAllocationRoutes = require('./api/pickUpAllocation')
const nonBearerTokenCheck = require('./../middleware/nonbearerTokenCheck')


module.exports = () => {
  const router = Router();
  router.use(nonBearerTokenCheck);

    userRoutes(router, passport);
    pickUpRoutes(router, passport);
    pickUpAllocationRoutes(router, passport);

    return router;
}