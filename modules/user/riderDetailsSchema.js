const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const riderDetailsSchema = new mongoose.Schema(
  {
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    license_number: { type: Number, required: true },
    bike_number: { type: String, required: true },
    bike_model: { type: String, required: true, },
    added_by: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false},
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    underscore: true,
  },
);

module.exports = mongoose.model('RiderDetails', riderDetailsSchema);