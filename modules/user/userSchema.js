const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema(
  {
    firstname: { type: String, },
    lastname: { type: String, },
    email: { type: String, required: true, },
    password: { type: String, },
    role: { type: String, enum: ['super-admin', 'admin', 'rider', 'merchant', 'customer'], },
    last_login_at: { type: Date, default: Date.now(), },
    isActive: { type: Boolean, default: true, },
    auth_provider: { type: String, default: 'Kedu', },
    phone: { type: String, default: '' },
    pan_number: { type: String, required: false, default: null },
    image: { type: String }
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    underscore: true,
  },
);

userSchema.pre('save', function (next) {
  if (!this.isModified('password') || !this.password) {
    return next();
  }
  this.password = bcrypt.hashSync(this.password, 10);
  next();
});

userSchema.methods.comparePassword = function (candidatePassword, cb) {
  if (!candidatePassword || !this.password) {
    return false;
  }
  return bcrypt.compare(candidatePassword, this.password);
};
module.exports = mongoose.model('User', userSchema);