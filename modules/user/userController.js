const userSch = require('./userSchema');
const riderSch = require('./riderDetailsSchema');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const responseHelper = require('../../helper/responseHelper');
const config = require('./userConfig');
const configs = require('./../../config')
const tokenGenerator = require('../../middleware/tokenGenerator');
const moment = require('moment');
const axios = require('axios');
const { nodemailer } = require('./../../helper/nodemailer');
const otpSch = require('./otpSchema');
const accessTokenSch = require('./accessToken');
const refreshTokenSch = require('./refreshToken');
const user = {};

user.addUser = async (req, res, next) => {
  try {
    const userDetails = new userSch({ email: "superadmin@gmail.com", password: 'Password@1234', role: 'super-admin' });

    let user = await userDetails.save();
    user = user.toObject();
    delete user['password'];
    const tokens = await tokenGenerator(user);
    responseHelper.sendResponse(res, httpStatus.OK, true, null, null, "success", tokens);
  } catch (err) {
    next(err);
  }
};

// @desc admin login
user.login = async (req, res, next) => {
  try {
    let user = await userSch.findOne({
      email: req.body['email'].trim(),
    });
    const sendTokenResponse = async () => {
      user['last_login_at'] = moment();
      user = user.toObject();
      delete user['password'];
      const tokens = await tokenGenerator(user);
      responseHelper.sendResponse(res, httpStatus.OK, true, null, null, config.loginSuccess, tokens);
    };

    user ?
      await user.comparePassword(req.body['password']) ?
        await sendTokenResponse() :
        await responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, null, "Enter correct password", "") :
      await responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, null, "User not found", "");

  } catch (err) {
    next(err)
  }
};

user.signUp = async (req, res, next) => {
  try {
    let user = new userSch({
      firstname: req.body['firstname'],
      lastname: req.body['lastname'],
      email: req.body['email'].trim(),
      role: req.body['role'].trim(),
      phone: req.body['phone'],
      password: req.body['password'],
      pan_number: req.body['pan_number']
    });


    await user.save();
    user = user.toObject();
    delete user['password'];

    const tokens = await tokenGenerator(user);

    responseHelper.sendResponse(res, httpStatus.OK, true, null, null, "success", tokens);
  } catch (err) {
    next(err);
  }
},

  //admin create managerial users
  user.createUser = async (req, res, next) => {
    try {
      const role = req.user.authUser['role'] == 'super-admin' ? 'admin' : 'rider';
      /* Creating password for managerial user */
      let password = generatePassword()

      let user = new userSch({
        firstname: req.body['firstname'],
        lastname: req.body['lastname'],
        email: req.body['email'].trim(),
        role: role.trim(),
        phone: req.body['phone'],
        password: password
      });

      await user.save();
      user = user.toObject();
      delete user['password'];

      if(req.user.authUser['role'] == 'admin'){
        let riderDetails = new riderSch({
          user_id : user['_id'],
          license_number : req.body['license_number'],
          bike_model : req.body['bike_model'],
          bike_number : req.body['bike_number'],
          added_by : req.user.authUser['_id']
        })
        await riderDetails.save();
      }

      const tokens = await tokenGenerator(user);
      await nodemailer({ user, password }, 'sendPassword', res)

      responseHelper.sendResponse(res, httpStatus.OK, true, null, null, "success", tokens);
    } catch (err) {
      next(err);
    }
  },

  user.otpVerification = async (req, res, next) => {
    try {
      const otpValidationCheck = await otpSch.findOne({ user_id: req.body.user_id, otp: req.body.otp })
      otpValidationCheck ?
        await userSch.findOneAndUpdate({ _id: req.body.user_id }, { $set: { otp_verified: true } }) :
        responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, null, "Invalid otp", null);
      responseHelper.sendResponse(res, httpStatus.OK, true, null, null, "success", null);
    } catch (err) {
      next(err);
    }
  }

user.logout = async (req, res, next) => {
  try {
    const user_id = req.user.authUser['_id']
    await userSch.findOneAndUpdate({ _id: user_id }, { $set: { active: false } });
    await accessTokenSch.remove({ access_token_id: req.user.authToken['access_token_id'] });
    await refreshTokenSch.remove({ access_token_id: req.user.authToken['access_token_id'] });
    responseHelper.sendResponse(res, httpStatus.OK, true, null, null, "success", null);
  } catch (error) {
    next(error)
  }
}

user.userList = async (req, res, next) => {
  try {
    let page;
    let size;
    let searchq = {};
    let sortq = '-_id';
    let selectq;
    let populate;
    let { role, firstname, email } = req.query;
    if (req.query.page && !isNaN(req.query.page) && req.query.page != 0) {
      page = Math.abs(req.query.page);
    } else {
      page = 1;
    }
    if (req.query.size && !isNaN(req.query.size) && req.query.size != 0) {
      size = Math.abs(req.query.size);
    } else {
      size = 10;
    }

    role ? searchq['role'] = { $regex: role, $options: "i" } : searchq = searchq;
    firstname ? searchq['firstname'] = { $regex: firstname, $options: "i" } : searchq = searchq;
    email ? searchq['email'] = { $regex: email, $options: "i" } : searchq = searchq;
    selectq = '-password';
    let datas = await responseHelper.getquerySendResponse(userSch, page, size, sortq, searchq, selectq, next, populate);

    return responseHelper.paginationSendResponse(res, httpStatus.OK, true, datas.data, config.get, page, size, datas.totaldata);

  } catch (err) {
    next(err)
  }
};

function generatePassword() {
  const chars = '0123456789abcdefghijklmnopqrstuvwxyz';
  let password = '';
  for (let i = 0; i < 8; i++) {
    password += chars[Math.floor(Math.random() * chars.length)];
  }
  return password;
}


function generateOTP() {
  const digits = '0123456789';
  let OTP = '';
  for (let i = 0; i < 4; i++) {
    OTP += digits[Math.floor(Math.random() * 10)];
  }
  return OTP;
}

module.exports = user;
