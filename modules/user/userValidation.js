const httpStatus = require('http-status');
const config = require('./userConfig');
const responseHelper = require('./../../helper/responseHelper');
const otherHelper = require('./../../helper/validationHelper');
const userSch = require('./userSchema');
const isEmpty = require('./../../validation/isEmpty');
const Joi = require('@hapi/joi');
const joiValidator = require('./../../helper/joiObjectValidator');

const validation = {};

validation.sanitizeRegister = (req, res, next) => {
  const sanitizeArray = [
    {
      field: 'email',
      sanitize: {
        trim: true,
      },
    },
    {
      field: 'phone',
      sanitize: {
        trim: true,
      },
    },
    {
      field: 'firstname',
      sanitize: {
        trim: true,
      },
    },
    {
      field: 'lastname',
      sanitize: {
        trim: true,
      },
    },
  ];
  otherHelper.sanitize(req, sanitizeArray);
  req.body.email = req.body.email ? req.body.email.toLowerCase() : '';
  next();
};

validation.checkDataDuplicate = async (req, res, next) => {
  try {
    const { email } = req.body;

    let filter = { email: email };

    if (req.params.id) {
      filter._id = { $ne: req.params.id }
    }

    const data = await userSch.findOne(filter);
    const error = {}; ``

    if (data) {
      error.email = 'Email already exists';
      return responseHelper.sendResponse(res, httpStatus.CONFLICT, false, null, error, config.validate.duplicateData, null);
    }
    next();
  }
  catch (err) {
    next(err);
  }
};

validation.validateRegisterInput = async (req, res, next) => {
  const data = req.body;
  let validateArray = [];
  if(req.user.authUser['role'] == 'admin'){
    validateArray = [
      {
        field: 'email',
        validate: [
          {
            condition: 'IsEmpty',
            msg: config.validate.empty,
          },
          {
            condition: 'IsEmail',
            msg: config.validate.isEmail,
          },
        ],
      },
      {
        field: 'phone',
        validate: [
          {
            condition: 'IsEmpty',
            msg: config.validate.empty,
          },
          {
            condition: 'IsPhone',
            msg: config.validate.invalid,
          },
        ],
      },
      {
        field: 'firstname',
        validate: [
          {
            condition: 'IsEmpty',
            msg: config.validate.empty,
          },
        ],
      },
      {
        field: 'lastname',
        validate: [
          {
            condition: 'IsEmpty',
            msg: config.validate.empty,
          },
        ],
      },
      {
        field: 'license_number',
        validate: [
          {
            condition: 'IsEmpty',
            msg: config.validate.empty,
          },
          {
            condition: 'IsInt',
            msg: config.validate.invalidNumber,
            option: { min: 0 }
          },
        ],
      },
      {
        field: 'bike_number',
        validate: [
          {
            condition: 'IsEmpty',
            msg: config.validate.empty,
          },
        ],
      },
      {
        field: 'bike_model',
        validate: [
          {
            condition: 'IsEmpty',
            msg: config.validate.empty,
          },
        ],
      },
    ];
  }
  validateArray = [
    {
      field: 'email',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
        {
          condition: 'IsEmail',
          msg: config.validate.isEmail,
        },
      ],
    },
    {
      field: 'phone',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
        {
          condition: 'IsPhone',
          msg: config.validate.invalid,
        },
      ],
    },
    {
      field: 'firstname',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'lastname',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
  ]
  
  const errors = otherHelper.validation(data, validateArray);
  if (!isEmpty(errors)) {
    return responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, errors, config.validate.inerr, null);
  } else {

    let pattern = new RegExp(/[+-]/);
    if (pattern.test(req.body.email)) {
      return responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, { email: config.validate.invalid_chars }, 'input error', null);
    }
    next();
  }
};

validation.validateRegisterInputSelf = async (req, res, next) => {
  const data = req.body;
  const validateArray = [
    {
      field: 'email',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
        {
          condition: 'IsEmail',
          msg: config.validate.isEmail,
        },
      ],
    },
    {
      field: 'phone',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
        {
          condition: 'IsPhone',
          msg: config.validate.invalid,
        },
      ],
    },
    {
      field: 'firstname',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'lastname',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'role',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
        {
          condition: 'IsIn',
          msg: config.validate.invalidRole,
          option: ['customer', 'merchant'],
        },
      ],
    },
    {
      field: 'pan_number',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
  ];
  const errors = otherHelper.validation(data, validateArray);
  if (!isEmpty(errors)) {
    return responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, errors, config.validate.inerr, null);
  } else {

    let pattern = new RegExp(/[+-]/);
    if (pattern.test(req.body.email)) {
      return responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, { email: config.validate.invalid_chars }, 'input error', null);
    }
    next();
  }
};

validation.loginInput = async (req, res, next) => {
  try {
    let error = {};
    const emailCheck = await userSch.findOne({ email: req.body.email })

    const JoiSchema = await Joi.object({
      email: Joi.string().regex(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/).rule({ message: "Enter a valid email", }).required(),
      password: Joi.string().required(),
    });
    if (!emailCheck) {
      console.log('if, req.body')
      error['email'] = "User does not exist";
    }
    await joiValidator.validateData(req.body, JoiSchema, error, res, next);
  } catch (error) {
    next(error)
  }
}

validation.validateOtpInput = async (req, res, next) => {
  const data = req.body;
  const validateArray = [
    {
      field: 'otp',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'user_id',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
        {
          condition: 'IsMongoId',
          msg: config.validate.isMongoId,
        },
      ],
    },
  ];
  const errors = otherHelper.validation(data, validateArray);
  if (!isEmpty(errors)) {
    return responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, errors, config.validate.inerr, null);
  } else {

    let pattern = new RegExp(/[+-]/);
    if (pattern.test(req.body.email)) {
      return responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, { email: config.validate.invalid_chars }, 'input error', null);
    }
    next();
  }
};




module.exports = validation;