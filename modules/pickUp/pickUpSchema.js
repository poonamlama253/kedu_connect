const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pickUpSchema = new mongoose.Schema(
  {
    firstname: { type: String, required: true },
    lastname: { type: String, required: false },
    email: { type: String, required: false },
    package_type: { type: String, required: true },
    location: { type: String, required: true },
    landmark: { type: String, required: true },
    desription: { type: String, required: true },
    cod: { type: Number, required: true },
    phone: { type: String, required: true, },
    status: { type: String, enum: ['request', 'pickup', 'received'], default: 'request' },
    added_by: { type: Schema.Types.ObjectId, ref: 'User', required: false, },
    updated_by: { type: Schema.Types.ObjectId, ref: 'User', required: false, },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    underscore: true,
  });

module.exports = mongoose.model('PickUp', pickUpSchema);;