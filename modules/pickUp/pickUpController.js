const pickUpSch = require('./pickUpSchema');
const httpStatus = require('http-status');
const responseHelper = require('../../helper/responseHelper');
const config = require('./pickUpConfig');
const pickUpController = {};

pickUpController.createPickUpRequest = async (req, res, next) => {
  try {
    let pickUp = new pickUpSch({
      firstname: req.body['firstname'],
      lastname: req.body['lastname'],
      email: req.body['email'],
      package_type: req.body['package_type'],
      location: req.body['location'],
      landmark: req.body['landmark'],
      desription: req.body['desription'],
      cod: req.body['cod'],
      phone: req.body['phone'],
      added_by: req.user.authUser['_id']
    });

    await pickUp.save();
    responseHelper.sendResponse(res, httpStatus.OK, true, pickUp, null, "success", null);

  } catch (err) {
    next(err)
  }

};

pickUpController.pickUpList = async (req, res, next) => {
  try {
    let page;
    let size;
    let searchq = {};
    let sortq = '-_id';
    let selectq;
    let populate = {path: 'added_by', select: '-password' };
  
    let { search } = req.query;
    if (req.query.page && !isNaN(req.query.page) && req.query.page != 0) {
      page = Math.abs(req.query.page);
    } else {
      page = 1;
    }
    if (req.query.size && !isNaN(req.query.size) && req.query.size != 0) {
      size = Math.abs(req.query.size);
    } else {
      size = 10;
    }

    if (req.user.authUser['role'] == 'merchant') {
      searchq = { added_by: req.user.authUser["_id"] }
    }

    let datas = await responseHelper.getquerySendResponse(pickUpSch, page, size, sortq, searchq, selectq, next, populate);

    return responseHelper.paginationSendResponse(res, httpStatus.OK, true, datas.data, config.get, page, size, datas.totaldata);

  } catch (err) {
    next(err)
  }
};

pickUpController.pickUpDetails = async (req, res, next) => {
  try {
    const route = await pickUpSch.findOne({ _id: req.params.pickUp_id }).populate({ path: 'added_by', select: 'firstname lastname email phone' });

    return responseHelper.sendResponse(res, httpStatus.OK, true, route, null, config.get, null);
  } catch (err) {
    next(err);
  }

};

pickUpController.updatePickUpRequest = async (req, res, next) => {
  try {
    const pickUp = await pickUpSch.findOneAndUpdate({
      _id: req.params.pickUp_id
    }, {
      $set:
      {
        firstname: req.body['firstname'],
        lastname: req.body['lastname'],
        email: req.body['email'],
        package_type: req.body['package_type'],
        location: req.body['location'],
        landmark: req.body['landmark'],
        desription: req.body['desription'],
        cod: req.body['cod'],
        phone: req.body['phone'],
        updated_by: req.user.authUser['_id']
      }
    }
    );

    if (req.user.authUser['role'] == 'admin') {
      await pickUpSch.findOneAndUpdate({ _id: req.params.pickUp_id }, { $set: { status: req.body['status'] } });
    }
    else if (req.user.authUser['role'] == 'rider') {
      await pickUpSch.findOneAndUpdate({ _id: req.params.pickUp_id }, { $set: { status: req.body['status'] } });
    }

    responseHelper.sendResponse(res, httpStatus.OK, true, req.body, null, config.patch, null);

  } catch (err) {
    next(err)
  }

};

pickUpController.deletePickUpRequest = async (req, res, next) => {
  try {
    await pickUpSch.findByIdAndDelete({ _id: req.params.pickUp_id });

    responseHelper.sendResponse(res, httpStatus.OK, true, null, null, config.delete, null);

  } catch (err) {
    next(err)
  }

};
module.exports = pickUpController;