const httpStatus = require('http-status');
const config = require('./pickUpConfig');
const responseHelper = require('./../../helper/responseHelper');
const otherHelper = require('./../../helper/validationHelper');
const pickUpSch = require('./pickUpSchema');
const isEmpty = require('./../../validation/isEmpty');

const validation = {};

validation.sanitizeRegister = (req, res, next) => {
  const sanitizeArray = [
    {
      field: 'email',
      sanitize: {
        trim: true,
      },
    },
    {
      field: 'phone',
      sanitize: {
        trim: true,
      },
    },
    {
      field: 'firstname',
      sanitize: {
        trim: true,
      },
    },
    {
      field: 'lastname',
      sanitize: {
        trim: true,
      },
    },
  ];
  otherHelper.sanitize(req, sanitizeArray);
  req.body.email = req.body.email ? req.body.email.toLowerCase() : '';
  next();
};



validation.validateInput = async (req, res, next) => {
  const data = req.body;
  const validateArray = [
    {
      field: 'firstname',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'package_type',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'phone',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
        {
          condition: 'IsPhone',
          msg: config.validate.invalid,
        },
      ],
    },
    {
      field: 'location',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'landmark',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'desription',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
      ],
    },
    {
      field: 'cod',
      validate: [
        {
          condition: 'IsEmpty',
          msg: config.validate.empty,
        },
        {
          condition: 'IsInt',
          msg: config.validate.invalidNumber,
          option: { min: 0 }
        },
      ],
    },
  ];
  const errors = otherHelper.validation(data, validateArray);
  if (!isEmpty(errors)) {
    return responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, errors, config.validate.inerr, null);
  } else {

    let pattern = new RegExp(/[+-]/);
    if (pattern.test(req.body.email)) {
      return responseHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, { email: config.validate.invalid_chars }, 'input error', null);
    }
    next();
  }
};




module.exports = validation;