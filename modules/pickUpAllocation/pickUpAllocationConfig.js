module.exports = {
  validate: {
    invalid: 'Invalid entry',
    empty: 'This field is required',
    isMongoId: 'This is not mongo id',
    isDate: 'This field must conatain Date.',
    duplicateData: 'Band already exists..',
    invalidInput: 'Invalid input',
    descriptionLength: 'length of description should be between 1 to 500 charaters',
  },
  get: 'Data obtaied successfully',
  post: 'Data stored successfully',
  patch: 'Data updated successfully',
  delete: 'Data deleted successfully'

}