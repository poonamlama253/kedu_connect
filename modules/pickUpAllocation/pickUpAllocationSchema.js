const mongoose = require('mongoose');

const pickUpAllocationSchema = new mongoose.Schema(
  {
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    pickUp_id: { type: mongoose.Schema.Types.ObjectId, ref: 'PickUp', required: true},
    added_by: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false},
  },
  {
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'},
    underscore: true,
  },
);

module.exports = mongoose.model('pickUpAllocation', pickUpAllocationSchema);
