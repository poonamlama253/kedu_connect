const pickUpAllocationSch = require('./pickUpAllocationSchema');
const httpStatus = require('http-status');
const responseHelper = require('../../helper/responseHelper');
const config = require('./pickUpAllocationConfig');

const pickUpAllocationController = {};

//@desc assign ticked validator for vehicle/bus
pickUpAllocationController.assignRider = async (req, res, next) => {
  try {
    let riderDetails = new pickUpAllocationSch({ 
      user_id: req.body['user_id'],
      added_by: req.user.authUser['_id'], 
      pickUp_id: req.params['pickUp_id'] 
    });
    await riderDetails.save(); 

    return responseHelper.sendResponse(res, httpStatus.OK, true, riderDetails, null, config.post, null);
  } catch (err) {
    next(err);
  }
};

pickUpAllocationController.pickUpAllocationList = async (req, res, next) => {
  try {
    let page;
    let size;
    let searchq = {};
    let sortq = '-_id';
    let selectq;
    let populate = {path: 'user_id pickUp_id added_by', select: '-password' };
    let { search } = req.query;
    if (req.query.page && !isNaN(req.query.page) && req.query.page != 0) {
      page = Math.abs(req.query.page);
    } else {
      page = 1;
    }
    if (req.query.size && !isNaN(req.query.size) && req.query.size != 0) {
      size = Math.abs(req.query.size);
    } else {
      size = 10;
    }

    if (req.user.authUser['role'] == 'rider') {
      searchq = { user_id: req.user.authUser["_id"] }
    }

    let datas = await responseHelper.getquerySendResponse(pickUpAllocationSch, page, size, sortq, searchq, selectq, next, populate);

    return responseHelper.paginationSendResponse(res, httpStatus.OK, true, datas.data, config.get, page, size, datas.totaldata);

  } catch (err) {
    next(err)
  }
};

pickUpAllocationController.pickUpAllocationDetails = async (req, res, next) => {
  try {
    const route = await pickUpAllocationSch.findOne({ _id: req.params.pickUpAllocation_id }).populate({ path: 'added_by user_id pickUp_id', select: 'firstname lastname email phone package_type location landmark desription cod status' });

    return responseHelper.sendResponse(res, httpStatus.OK, true, route, null, config.get, null);
  } catch (err) {
    next(err);
  }

};

pickUpAllocationController.updatePickUpAllocation = async (req, res, next) => {
  try {
    await pickUpAllocationSch.findOneAndUpdate({_id: req.params.pickUpAllocation_id}, {$set:{
      user_id: req.body['user_id'], 
      pickUp_id: req.body['pickUp_id'], 
      updated_by: req.user.authUser['_id']
    }});

    responseHelper.sendResponse(res, httpStatus.OK, true, req.body, null, config.patch, null);

  } catch (err) {
    next(err)
  }

};

pickUpAllocationController.deletePickUpAllocation = async (req, res, next) => {
  try {
    await pickUpAllocationSch.findByIdAndDelete({ _id: req.params.pickUpAllocation_id });

    responseHelper.sendResponse(res, httpStatus.OK, true, null, null, config.delete, null);

  } catch (err) {
    next(err)
  }

};

module.exports = pickUpAllocationController;
