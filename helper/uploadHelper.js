const multer = require('multer');
const uploader = {};


//@desc define storage file
const storage = multer.diskStorage({

  destination: (req, file, cb) => {

    if (file.fieldname === "pic") {
      cb(null, './public/profilePhoto');
    } else {
      cb(null, './public/productPhoto');
    }

  },

  filename: (req, file, cb) => {
    cb(null, Date.now() + file.originalname)
  }

});

//@desc validations
const fileFilter = (req, file, cb) => {
  if (!file.originalname.match(/\.(jpg|JPG|jpeg|png|PNG|gif|JPEG)$/)) {
    return cb(new Error("File format not supported! "), false);
  }
  cb(null, true);
};

module.exports  = multer({ storage: storage, fileFilter: fileFilter });

