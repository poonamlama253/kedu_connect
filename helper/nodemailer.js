const nodemailer = require('nodemailer');
const { sendSignUpCode, sendPassword } = require('./nodemailerHelper');

module.exports = {
  nodemailer: async (requirements, forWhat, res) => {
    try {
      const transporter = await nodemailer.createTransport({
        service: process.env.MAIL_SERVICE,
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        auth: {
          user: process.env.MAIL_ACCOUNT,
          pass: process.env.MAIL_PASSWORD,
        },
      });

      console.log('transporter', transporter);

      switch (forWhat) {
        case 'sendCode':
          return await sendSignUpCode(requirements, transporter, res);
          break;
        case 'sendPassword':
          return await sendPassword(requirements, transporter, res);
          break;
      }
    } catch (err) {
      next(err);
    }
  },
};
