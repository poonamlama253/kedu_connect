const ejs = require('ejs');

module.exports = {

  sendSignUpCode: async (requirements, transporter, res) => {
    try {
      const data = await ejs.renderFile('./public/ejs/sendSignupCode.ejs', {
        name: requirements.user.firstname,
        email: requirements.user.email,
        code: requirements.otp_code,
      });

      if (data) {
        const mainOptions = {
          from: `"${process.env.MAIL_NAME}" <${process.env.MAIL_FROM}>`,
          to: requirements.user.email,
          subject: 'Sign up code',
          html: data,
        };
        await transporter.sendMail(mainOptions);
        console.log('OTP sent to the authorizedd user');
        return 0;
      } else {
        console.log('ejsFunction error');
      }
    } catch (err) {
      console.log(err);
    }
  },

  sendPassword: async (requirements, transporter, res) => {
    try {
      const data = await ejs.renderFile('./public/ejs/sendPassword.ejs', {
        name: requirements.user.firstname,
        email: requirements.user.email,
        password: requirements.password,
      });

      if (data) {
        const mainOptions = {
          from: `"${process.env.MAIL_NAME}" <${process.env.MAIL_FROM}>`,
          to: requirements.user.email,
          subject: 'Send Password',
          html: data,
        };
        await transporter.sendMail(mainOptions);
        console.log('Password sent to the authorizedd user');
        return 0;
      } else {
        console.log('ejsFunction error');
      }
    } catch (err) {
      console.log(err);
    }
  },

};